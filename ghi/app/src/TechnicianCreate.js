import React, { useState } from "react"


function TechnicianCreate() {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = firstName
    data.last_name = lastName;
    data.employee_id = employeeId;

    const url = 'http://localhost:8080/api/technicians/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const techResponse = await fetch(url, fetchOptions);
    if (techResponse.ok) {
      setFirstName('');
      setLastName('');
      setEmployeeId('');
      setCreatedTech(true)
      setTimeout(() => setCreatedTech(false), 3000)
    } else {
      console.log(`techResponse error: ${JSON.stringify(techResponse)}`)
    }
  }

  const handleSetFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleSetLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleSetEmployeeId = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  }

  const [createdTech, setCreatedTech] = useState(false);

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (createdTech) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-tech-form">
                <h1 className="card-title">Add a Technician</h1>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleSetFirstName} value={firstName} required placeholder="First name" type="text" id="first_name" name="first_name" className="form-control" />
                      <label htmlFor="first_name">Technician First Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleSetLastName} value={lastName} required placeholder="Last name" type="text" id="last_name" name="last_name" className="form-control" />
                      <label htmlFor="last_name">Technician Last Name</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleSetEmployeeId} value={employeeId} required placeholder="Employee ID" type="text" id="employee_id" name="employee_id" className="form-control" />
                      <label htmlFor="name">Employee ID</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Add Technician</button>
              </form>
              <div className={messageClasses} id="success-message">
                Technician added to employee records.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TechnicianCreate
