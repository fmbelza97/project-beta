import React, { useEffect, useState } from "react"

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([])

  async function loadAutomobiles() {
    const response = await fetch("http://localhost:8100/api/automobiles");
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos)
    }
  }

  useEffect(() => {
    loadAutomobiles();
  }, []);

  async function handleDeleteAutomobile(href) {
    const automobileUrl = `http://localhost:8100${href}`
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const automobileResponse = await fetch(automobileUrl, fetchOptions);
    if (automobileResponse.ok) {
      loadAutomobiles();
    }
  }

  return (
    <div className="container my-5">
      <table className="table">
        <thead>
          <tr className="bg-primary">
            <th></th>
            <th>vin</th>
            <th>color</th>
            <th>year</th>
            <th>model</th>
            <th>manufacturer</th>
            <th>sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles && automobiles.map(automobile => {
            return (
              <tr className="table-info" key={automobile.href} value={automobile.href}>
                <td>
                  <button className="btn btn-sm btn-primary"
                    onClick={() => handleDeleteAutomobile(automobile.href)}>
                    Delete
                  </button>
                </td>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
                <td>{automobile.sold ? "Yes" : "No"}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )

}

export default AutomobileList
