from django.contrib import admin
from .models import Appointment, Technician

admin.site.register(Appointment)
admin.site.register(Technician)
